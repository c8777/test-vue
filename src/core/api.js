class Api {
  constructor(url = ''){
    this.url = url
  }

  async get(url = '', query) {
    let currentUrl = this.url + url;

    if (query) {
      currentUrl += '?' + new URLSearchParams(query);
    }

    const res = await fetch(currentUrl);
    return res.json()
  }
}

export const api = new Api('https://api.openweathermap.org');
